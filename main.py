# -*- coding: utf-8 -*-
import os

__author__ = 'alexey'

from PyQt4.QtCore import pyqtSlot
from SpeechFunctions import *
import sys
from PyQt4.QtGui import *
import pyglet
import pygame
from pydub import AudioSegment # also need FFmpeg on system

reload(sys)
sys.setdefaultencoding('utf-8')



print 'Добро пожаловать в программу, как ваше ничего?'

# speechSynthesizer = SpeechSynthesizer()
# speechSynthesizer.synthesText("Привет еще раз!")

#speechSynthesizer.openSoundFileOld("Добрый день!.mp3")


#creating gui form
def main():

    app = QApplication(sys.argv)

    widget = QWidget()
    widget.resize(640,480)
    widget.setWindowTitle("SpeechApplication")


    tbTextToSpeech = QTextEdit(widget)
    # tbTextToSpeech.resize(280,150)

    formLayout = QFormLayout()
    formLayout.setSpacing(10)
    formLayout.addRow(QLabel("Text to Speech"),tbTextToSpeech )




    ttsBtn = QPushButton("Synthes text to speech", widget)
    ttsBtn.setToolTip(("Synthes text to speech"))

    @pyqtSlot()
    def on_click_tts():
        textToSpeech = tbTextToSpeech.toPlainText()
        speechSynthesizer = SpeechSynthesizer()
        speechSynthesizer.synthesText(textToSpeech)



    ttsBtn.clicked.connect(on_click_tts)
    # getTextBtn.resize(getTextBtn.sizeHint())
    # button.move(20, 100)
    formLayout.addRow(ttsBtn)


    reformatSoundBtn = QPushButton("Reformat to WAVE file", widget)
    reformatSoundBtn.setToolTip("Reformat to WAVE file")

    @pyqtSlot()
    def on_click_reformat_to_wav():
        # Get filename using QFileDialog
        filePath = QFileDialog.getOpenFileName(widget, 'Reformat File', '/', "Music Files (*.mp3 *.wav *.flac)")
        # filePath = str(filePath)
        fileWorker = FileWorker()
        fileWorker.reformatToWavFile(filePath)

    reformatSoundBtn.clicked.connect(on_click_reformat_to_wav)

    playSoundBtn = QPushButton("Play WAVE file", widget)
    playSoundBtn.setToolTip("Play WAVE file")

    @pyqtSlot()
    def on_click_play_wav_file():
        filePath = QFileDialog.getOpenFileName(widget, 'Open File', '/', "Music Files (*.wav)")
        fileWorker = FileWorker()
        fileWorker.playWavFile(filePath)
    playSoundBtn.clicked.connect(on_click_play_wav_file)

    formLayout.addRow(reformatSoundBtn, playSoundBtn)
    widget.setLayout(formLayout)



    widget.show()
    app.exec_()

if __name__ == '__main__':
    main()
