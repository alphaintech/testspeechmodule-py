# -*- coding: utf-8 -*-
import os
from pydub import AudioSegment
import pygame

__author__ = 'alexey'
import requests
from playsound import playsound


class SpeechSynthesizer:
    def __init__(self):
        pass

    def synthesText(self, text):
        yandexAPI = YandexAPI()
        yandexAPI.synthesText(text)

        #print "SynthesText called with", text

    def openSoundFileOld(self, name):
        fileWorker = FileWorker()
        fileWorker.openSoundFile(name)

class YandexAPI:
    def __init__(self):
        pass

# api documentation
#https://tech.yandex.ru/speechkit/cloud/doc/guide/concepts/tts-http-request-docpage/#format-desc
#
    def synthesText(self, text, neededFormat=None, neededLang=None,neededSpeaker=None, neededSpeed=None,neededEmotion=None):
        if neededFormat is None: neededFormat="wav"
        if neededLang is None: neededLang="ru-RU"
        if neededSpeaker is None: neededSpeaker="oksana"
        if neededSpeed is None: neededSpeed="0.9"
        if neededEmotion is None: neededEmotion="good"

        url = self.createGetRequestUrl(text, neededFormat, neededLang, neededSpeaker, neededSpeed, neededEmotion)
        soundResponse = self.getRequest(url)


        fileWorker = FileWorker()
        fileWorker.saveSoundFile(text,soundResponse.content, neededFormat)


        print "SynthesText called with", text, "from YandexAPI"

    def createGetRequestUrl(self, text, neededFormat, neededLang,neededSpeaker, neededSpeed,neededEmotion):
        baseUrl = "https://tts.voicetech.yandex.net/generate?"
        reqText = "text=" + text                        #Ограничение на длину строки: 2000 байт.
        respFormat = "&format=" + neededFormat          #mp3 , wav, opus
        respLang = "&lang=" + neededLang                #ru-RU , en-US , (+ ukraine and turkish)
        respSpeaker = "&speaker=" + neededSpeaker       #женские голоса: jane, oksana, alyss и omazh; мужские голоса: zahar и ermil.
        respSpeed = "&speed=" + neededSpeed             #from 0.1 to 3.0
        respEmotion = "&emotion=" + neededEmotion       #good — радостный, доброжелательный;evil — раздраженный;neutral — нейтральный (используется по умолчанию).
        apiKey = "&key=" + "8414ced7-efea-437e-9d6e-a3d0f4acfe00"
        url = baseUrl + reqText + respFormat + respLang + respSpeaker + respSpeed + respEmotion + apiKey
        return url




    def getRequest(self, url):
        #print url
        response = requests.get(url)
        return response


class FileWorker:
    def __init__(self):
        pass

    def saveSoundFile(self, nameWithFormat, soundContent):
        soundFile = open("./soundFiles/"+nameWithFormat, "w")
        soundFile.write(soundContent)

    def saveSoundFile(self, name, soundContent, fileFormat):
        soundFile = open("./soundFiles/"+name + "."+fileFormat, "w")
        soundFile.write(soundContent)

    def openSoundFile(self, name):
        with open("./soundFiles/"+name, "r") as contentOfFile:
            content = contentOfFile.read()
        self.saveSoundFile(name, content, "wav")

    def reformatToWavFile(self, filePath):
        filePath = str(filePath)
        fileName, fileExtantion = os.path.splitext(filePath)
        # print fileName,"^^^", fileExtantion
        soundFile = AudioSegment.from_file(filePath, fileExtantion[1:])
        # song = AudioSegment.from_mp3(filename)
        soundFile.export(fileName+".wav", format="wav")

    def playWavFile(self, filePath):
        filePath = str(filePath)
        fileName, fileExtantion = os.path.splitext(filePath)
        with open(filePath, "r") as contentOfFile:
            content = contentOfFile.read()
        pygame.init()
        pygame.mixer.Sound(content).play()